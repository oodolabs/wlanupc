import {RPC} from "roal";
import NobleTransport from "roal-transport-noble/lib/transport";
import {WiFiService} from "./service";

export function connect() {
  const transport = new NobleTransport({
    serviceUUID: 'F010',
    characteristicUUID: 'F011'
  });
  const rpc = RPC.create(transport);
  const service = new WiFiService(rpc);
  return {transport, rpc, service}
}
