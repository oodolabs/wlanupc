import {RPC} from "roal";
import {Hotspot, Network} from "wirelesser/lib/wpa";

export interface WiFiCreds {
  ssid: string;
  key: string;
}

export class WiFiService {
  protected _rpc: RPC;

  constructor(rpc: RPC) {
    this._rpc = rpc;
  }

  async ping() {
    return this._rpc.request('ping');
  }

  async scan(): Promise<Hotspot[]> {
    return <Hotspot[]>await this._rpc.request('scan');
  }

  async list(): Promise<Network[]> {
    return <Network[]> await this._rpc.request('list');
  }

  async remove(id) {
    return await this._rpc.request('remove', id);
  }

  async connect(creds: WiFiCreds) {
    return await this._rpc.request('connect', creds);
  }

  async connectAndWait(creds: WiFiCreds) {
    return await this._rpc.request('connectAndWait', creds);
  }

  async disconnect() {
    return await this._rpc.request('disconnect');
  }

  async acknowledge() {
    return this._rpc.request('acknowledge');
  }

  async ip() {
    return this._rpc.request('ip');
  }

  async status() {
    return this._rpc.request('status');
  }

  async nid() {
    return this._rpc.request('nid');
  }
}
