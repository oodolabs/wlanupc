import program = require("caporal");
import {connect} from "./client";

const pkg = require("../package.json");

export function cli(argv) {
  program.version(pkg.version);
  program.command('ping', 'Ping the config service')
    .action(async () => {
      const {service, transport} = connect();
      console.log(await service.ping());
      await transport.close();
      process.exit(0);
    });
  program.command('list', 'List stored WiFi networks')
    .action(async () => {
      const {service, transport} = connect();
      console.log(await service.list());
      await transport.close();
      process.exit(0);
    });
  program.command('remove <id>', 'Remove stored WiFi network')
    .action(async (id) => {
      const {service, transport} = connect();
      console.log(await service.remove(id));
      await transport.close();
      process.exit(0);
    });
  program.command('scan', 'Scan WiFi networks')
    .action(async () => {
      const {service, transport} = connect();
      console.log(await service.scan());
      await transport.close();
      process.exit(0);
    });
  program.command('ip', 'Get wlan ip address')
    .action(async () => {
      const {service, transport} = connect();
      console.log(await service.ip());
      await transport.close();
      process.exit(0);
    });
  program.command('connect', 'Connect WiFi network')
    .alias('c')
    .argument('<ssid>', 'WiFi ssid')
    .argument('<password>', 'WiFi password')
    .option('-w, --wait', 'Wait for the connectivity')
    .action(async ({ssid, password}, options, logger) => {
      const {service, transport} = connect();
      try {
        if (options.wait) {
          logger.info('connectAndWait');
        } else {
          logger.info('connect');
        }
        const creds = {ssid, key: password};
        const result = options.wait ? await service.connectAndWait(creds) : await service.connect(creds);
        console.log(result);
      } catch (e) {
        console.error('error', e);
      }

      await transport.close();
      process.exit(0);
    });
  program.command('disconnect', 'Disconnect form current WiFi network')
    .action(async () => {
      const {service, transport} = connect();
      console.log(await service.disconnect());
      await transport.close();
      process.exit(0);
    });
  program.command('status', 'Get status of wlan')
    .action(async () => {
      const {service, transport} = connect();
      console.log(await service.status());
      await transport.close();
      process.exit(0);
    });
  program.command('nid', 'Get node id | serial number')
    .action(async () => {
      const {service, transport} = connect();
      console.log(await service.nid());
      await transport.close();
      process.exit(0);
    });

  program.parse(argv);
}
