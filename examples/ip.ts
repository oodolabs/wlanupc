import {connect} from "..";

(async () => {
  try {
    const {service} = connect();
    const result = await service.ip();
    console.log(result);
  } catch (e) {
    console.error(e);
  }

})();

