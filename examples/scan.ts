import {connect} from "../src";

(async () => {
  const {service} = connect();
  const result = await service.scan();
  console.log(result);
})();

