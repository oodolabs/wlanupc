import {connect} from "../src";

(async () => {
  const {service} = connect();
  const result = await service.ping();
  console.log(result);
})();
